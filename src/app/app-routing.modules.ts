import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ExtratoComponent } from "./extrato/extrato.component";
import { NovaTransferenciaComponent } from "./nova-transferencia/nova-transferencia/nova-transferencia.component";

export const routes: Routes = [
  { path: 'extrato', component: ExtratoComponent },
  { path: '', redirectTo:'extrato', pathMatch:'full' },
  { path: 'nova-transferencia', component: NovaTransferenciaComponent }
];
@NgModule(
  {
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
  }
)
export class AppRoutingModule { }
