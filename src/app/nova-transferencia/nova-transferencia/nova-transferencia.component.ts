import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Transferencia } from 'src/app/models/transferencia';
import { TransferenciaService } from './../../services/transferencia.service';


@Component({
  selector: 'app-nova-transferencia',
  templateUrl: './nova-transferencia.component.html',
  styleUrls: ['./nova-transferencia.component.scss']
})
export class NovaTransferenciaComponent {

  constructor(private transferenciaService: TransferenciaService, private router : Router) { }

  valor: number;
  destino: string;


  transferir() {
    console.log('Solicitada nova transferencia');
    const transferencia =  new Transferencia(this.destino,this.valor );
    this.transferenciaService.Adicionar(transferencia).subscribe(resultado => {
      console.log(resultado);
      this.limparCampos();
      this.router.navigateByUrl('extrato')
    },
     error => console.error(error)
    );
  }

  limparCampos() {
    this.valor = 0;
    this.destino = "";
  }
}
