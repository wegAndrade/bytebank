import { Injectable } from '@angular/core';
import { Transferencia } from '../models/transferencia';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class TransferenciaService {

  private listaTransferencias: Transferencia[];
  private url = 'http://localhost:3000/transferencias';
  constructor(private httpClient: HttpClient) {
    this.listaTransferencias = [];
  }

  get Transferencias() {
    return this.listaTransferencias;
  }

  todas(): Observable<Transferencia[]> {
    return this.httpClient.get<Transferencia[]>(this.url);
  }

  Adicionar(transferencia: Transferencia): Observable<Transferencia> {
    return this.httpClient.post<Transferencia>(this.url, transferencia)
  }

}
